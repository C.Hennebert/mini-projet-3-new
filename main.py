import argparse
import datetime
import sqlite3
import logging
import sys
import os
import csv
import atexit

#Initialize the database and table
def createDb(file):
    connection = sqlite3.connect(file)
    atexit.register(closeConnection, connection)
    logtext("INFO", "Initializing data base")
    c = connection.cursor()
    #SQLite command to count number of table with the name SIV
    tableExistCmd = "SELECT count(name) FROM sqlite_master WHERE type='table' AND name='SIV'"
    #SQLite command to create SIV table
    createTableCmd = "CREATE TABLE SIV (\
        adresse_titulaire TEXT, \
        nom TEXT, \
        prenom TEXT, \
        immatriculation TEXT UNIQUE, \
        date_immatriculation TEXT, \
        vin TEXT, \
        marque TEXT, \
        denomination_commerciale TEXT, \
        couleur TEXT, \
        carrosserie TEXT, \
        categorie TEXT, \
        cylindre TEXT, \
        energie TEXT, \
        places TEXT, \
        poids TEXT, \
        puissance TEXT, \
        type TEXT, \
        variante TEXT, \
        version TEXT)"

    #Verify is table SIV already exist and if not create her
    c.execute(tableExistCmd)
    if c.fetchone()[0] != 1:
        c.execute(createTableCmd)
        logtext("INFO", "Creating table SIV")
    else:
        logtext("INFO", "Table SIV already exist")
    connection.commit()
    return connection

#Add or update vehicle in the database
def addVehicle(line, connection):
    c = connection.cursor()
    #SQLite command to add or replace a vehicle in the database
    sendVehicleCmd = "INSERT OR REPLACE INTO SIV VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
    #SQLite command to check if the vehicle already exist
    vehicleExistCmd = "SELECT * FROM SIV WHERE immatriculation=?"
    #Check if the vehicle exist and create or replace him
    c.execute(vehicleExistCmd, [line[3]])
    if not c.fetchone():
        logtext("INFO", "Sending vehicle " + line[3] + " to data base")
    else:
        logtext("INFO", "Vehicle " + line[3] + " already exist, updating him")
    c.execute(sendVehicleCmd, line)
    connection.commit()

#Take line form the scv file and convert and return in a proper format
def convertLine(line, nbLine):
    neworder = [0, 9, 10, 8, 11, 4, 7, 12, 2, 3, 6, 1, 13, 14, 15, 16, 5]
    newline = [''] * 19
    if line.count != 17:
        logtext("ERROR", "Problem with line at index " + str(nbLine))
    for idx, param in enumerate(line):
        if idx == 15:
            newparam = param.split(', ')
            for count, info in enumerate(newparam):
                newline[neworder[idx] + count] = info
        elif idx == 5:
            try:
                newdate = datetime.datetime.strptime(param, '%Y-%m-%d')
                newline[neworder[idx]] = newdate.strftime('%d/%m/%Y')
            except:
                newline[neworder[idx]] = param
        else:
            newline[neworder[idx]] = param
    return newline

#Function for parsing arguments
def parseArgs():
    parser = argparse.ArgumentParser(description='Add vehicles from csv file to a database')
    parser.add_argument('--delim', required=False, default='|', type=str, help='delimiter for csv file')
    parser.add_argument('--database', required=True, type=str, help='data base file')
    parser.add_argument('--log', required=False, default="log.txt", type=str, help='logger file')
    parser.add_argument('--csv', required=True, type=str, help='csv file to parse and stock')
    return parser.parse_args()

#Read scv file and call convert line with each line then send the new line to the data base using addVehicle
def sendSplittedFileToDb(file, delim, connection):
    #Check if the file exist
    if not os.path.exists(file):
        logtext("ERROR", "Csv File doesn't exist or is unreadable")
        exit(1)
    logtext("INFO", "Start reading " + file + " file")
    with open(file, 'r') as csvfile:
        #Protect file closing by using atexit
        atexit.register(closeFile, csvfile)
        reader = csv.reader(csvfile, delimiter=delim)
        firstline = True
        nbLine = 1
        for line in reader:
            #Don't read the first line of the file
            if firstline == True:
                firstline = False
            else:
                newLine = convertLine(line, nbLine)
                addVehicle(newLine, connection)
            nbLine += 1
    csvfile.close()

def initLogger(file):
    logging.basicConfig(filename=file, format='%(asctime)s %(message)s', level=logging.DEBUG)

#Simplify logger function in one function
def logtext(textType, text):
    if textType == "ERROR":
        logging.getLogger().error(text)
        print("ERROR : " + text)
    elif textType == "INFO":
        logging.getLogger().info(text)

#Atexit function for connection to database
def closeConnection(connection):
    if connection:
        connection.close()

#Atexit function for scv file
def closeFile(file):
    if not file.closed:
        file.close()

def main():
    args = parseArgs()
    connection = createDb(args.database)
    initLogger(args.log)

    sendSplittedFileToDb(args.csv, args.delim, connection)
    connection.close()
main()
